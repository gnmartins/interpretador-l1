
(* TRABALHO FINAL DE SEMÂNTICA 2016/1
	
	Componentes do Grupo:
	Günter Matheus Hertz 	- 220491
	Leonardo Bissani		- 220485
	Paulo Ricardo Delwing	- 208783 *)

(* _________________________________________________________ *)

(*  ---------- Tipos ---------- *)
type variavel = string

type operador =
	| Soma
	| Subt
	| Mult
	| Div
	| Maior
	| MaiorIgual
	| Menor
	| MenorIgual
	| Igual
	| Diferente

type expressao =
	| Numero of int
	| Bool of bool
	| Op of expressao * operador * expressao
	| If of expressao * expressao * expressao
	| Var of variavel
	| Aplicacao of expressao * expressao
	| Lam of variavel * expressao
	| Let of variavel * expressao * expressao
	| LetRec of variavel * variavel * expressao * expressao

type valor =
	| ValorNumerico of int
	| ValorBooleano of bool
	| Vclos of variavel * expressao * ambiente
	| Vrclos of variavel * variavel * expressao * ambiente
and
	ambiente = (variavel * valor) list

(* _________________________________________________________ *)

(* exceptions *)
(* exception CantEvaluateExpression of string;; *)
exception CantEvaluateIf of string;;

(* helpers *)
exception CantGetValueFromNone of string ;;

exception CantGetValue of string;;
exception CantEvaluate of string;;

(* usado para que devolva o valor referente a um valor opcional, podendo agora ser calculado pelas
operações binarias *)
let get (x: valor option) : valor =
	match x with
	| None -> raise (CantGetValueFromNone "Valor indisponivel para None") 
	| Some (y) -> y;;

(* _________________________________________________________ *)

(*  ---------- Operadores ---------- *)
let soma (a: valor)(b: valor) : valor =
	ValorNumerico (
		(match a with
		| ValorNumerico (x) -> x
		| _ -> raise (CantGetValue "Sem valor"))
		+
		(match b with
		| ValorNumerico (y) -> y
		| _ -> raise (CantGetValue "Sem valor"))
	);;

let subt (a: valor)(b: valor) : valor =
	ValorNumerico (
		(match a with
		| ValorNumerico (x) -> x
		| _ -> raise (CantGetValue "Sem valor"))
		-
		(match b with
		| ValorNumerico (y) -> y
		| _ -> raise (CantGetValue "Sem valor"))
	);;

let mult (a: valor)(b: valor) : valor =
	ValorNumerico (
		(match a with
		| ValorNumerico (x) -> x
		| _ -> raise (CantGetValue "Sem valor"))
		*
		(match b with
		| ValorNumerico (y) -> y
		| _ -> raise (CantGetValue "Sem valor"))
	);;

let div (a: valor)(b: valor) : valor =
	ValorNumerico (
		(match a with
		| ValorNumerico (x) -> x
		| _ -> raise (CantGetValue "Sem valor"))
		/
		(match b with
		| ValorNumerico (y) -> y
		| _ -> raise (CantGetValue "Sem valor"))
	);;

let igual (a: valor)(b: valor) : valor =
	ValorBooleano (
		(match a with
		| ValorNumerico (x) -> x
		| _ -> raise (CantGetValue "Sem valor"))
		==
		(match b with
		| ValorNumerico (y) -> y
		| _ -> raise (CantGetValue "Sem valor"))
	);;

let diferente (a: valor)(b: valor) : valor =
	ValorBooleano (
		(match a with
		| ValorNumerico (x) -> x
		| _ -> raise (CantGetValue "Sem valor"))
		!=
		(match b with
		| ValorNumerico (y) -> y
		| _ -> raise (CantGetValue "Sem valor"))
	);;

let maior (a: valor)(b: valor) : valor =
	ValorBooleano (
		(match a with
		| ValorNumerico (x) -> x
		| _ -> raise (CantGetValue "Sem valor"))
		>
		(match b with
		| ValorNumerico (y) -> y
		| _ -> raise (CantGetValue "Sem valor"))
	);;

let menor (a: valor)(b: valor) : valor =
	ValorBooleano (
		(match a with
		| ValorNumerico (x) -> x
		| _ -> raise (CantGetValue "Sem valor"))
		<
		(match b with
		| ValorNumerico (y) -> y
		| _ -> raise (CantGetValue "Sem valor"))
	);;

let maiorigual (a: valor)(b: valor) : valor =
	ValorBooleano (
		(match a with
		| ValorNumerico (x) -> x
		| _ -> raise (CantGetValue "Sem valor"))
		>=
		(match b with
		| ValorNumerico (y) -> y
		| _ -> raise (CantGetValue "Sem valor"))
	);;

let menorigual (a: valor)(b: valor) : valor =
	ValorBooleano (
		(match a with
		| ValorNumerico (x) -> x
		| _ -> raise (CantGetValue "Sem valor"))
		<=
		(match b with
		| ValorNumerico (y) -> y
		| _ -> raise (CantGetValue "Sem valor"))
	);;

(* _________________________________________________________ *)

(* Ambiente *)
let rec lookup_ambiente (ambiente: ambiente) (x: variavel) : valor option = 
	match ambiente with
	| [] -> None
	| (variavel, valor)  :: tl -> 
		if compare variavel x == 0 then 
			Some valor 
		else 
			lookup_ambiente tl x;;

let rec removeVariavel (ambiente: ambiente) (x: variavel) : ambiente = 
	match ambiente with
	| [] -> []
	| (variavel, valor) :: tl ->
		if compare variavel x == 0 then 
			tl
		else
			(variavel, valor) :: (removeVariavel tl x);; 

let rec atualizaValor (ambiente: ambiente) (x: variavel) (v: valor) : ambiente =
	match ambiente with
	| [] -> []
	| (variavel, valor) :: tl -> 
		if compare variavel x == 0 then
			(variavel, v)::tl
		else
			(variavel, valor) :: (atualizaValor tl x v);;

let atualizaAmbiente (ambiente: ambiente) (x: variavel) (v: valor) : ambiente =
	(x,v)::ambiente

(* _________________________________________________________ *)

(* Semantica Operacional - Big- *)
let rec avalia (ambiente: ambiente) (e: expressao) : valor option =
	match e with
	| Numero e -> Some (ValorNumerico e)
	| Bool e -> Some (ValorBooleano e)
	| Var e -> lookup_ambiente ambiente e
	| If (e1, e2, e3) -> 
		( match avalia ambiente e1 with
		| Some (ValorBooleano true) -> avalia ambiente e2
		| Some (ValorBooleano false) -> avalia ambiente e3 
		| _ -> raise (CantEvaluateIf "Sintaxe Incorreta")
		)
	| Op (e1, op, e2) ->
		let v1 = get(avalia ambiente e1) in
		let v2 = get(avalia ambiente e2) in
		(match op with
			| Soma ->  Some ( soma v1 v2 )
			| Subt ->  Some ( subt v1 v2 )
			| Mult ->  Some ( mult v1 v2 )
			| Div ->  Some ( div v1 v2 )
			| Igual ->  Some ( igual v1 v2 )
			| Diferente ->  Some ( diferente v1 v2 );
			| Maior ->  Some ( maior v1 v2 );
			| MaiorIgual ->  Some ( maiorigual v1 v2 );
			| Menor ->  Some ( menor v1 v2 );
			| MenorIgual ->  Some ( menorigual v1 v2 );
		)
	| Lam (variavel, e1) ->
		Some (Vclos (variavel, e1, ambiente)
		)
	| Aplicacao (e1, e2) ->
    	let e1'= avalia ambiente e1 in
    	(match e1' with
    	| Some ( Vclos (variavel, expressao, ambiente')) -> 
    		(let e2' = avalia ambiente e2 in
    			( match e2' with
    				| Some v -> 
    					let ambienteAtualizado : ambiente = (atualizaAmbiente ambiente' variavel v) in
    					avalia ambienteAtualizado e
    				| _ -> raise (CantEvaluate "Impossivel avaliar")
    			)
    		)
    	| Some (Vrclos (f, x, e, ambiente')) -> 
    		(let e2' = avalia ambiente e2 in
    			( match e2' with 
    				| Some v ->
    					let ambienteAtualizado : ambiente = (atualizaAmbiente (atualizaAmbiente ambiente' x v) f (Vrclos(f, x, e, ambiente'))) in
    					avalia ambienteAtualizado e
    				| _ -> raise (CantEvaluate "Impossivel avaliar")
    			)
    		)
    	| _ -> None
    	)
	| Let (variavel, e1, e2) -> 
		let ambienteAtualizado : ambiente =
			(atualizaAmbiente ambiente variavel
				(match avalia ambiente e1 with
				| Some v -> v
				| _ -> raise (CantEvaluate "Impossivel Avaliar")
				)
			)
		in avalia ambienteAtualizado e2

	| LetRec (f, x, e1, e2) -> let closure = Vrclos(f, x, e1, ambiente) in
          (avalia (atualizaAmbiente ambiente f closure) e2)
      
    ;;

(* _________________________________________________________ *)

(* Limpando o ambiente *)
let empty ambiente : ambiente = [ ];;

print_string ("---------------------------");;
print_string ("\n\tTestes:\n");;
print_string ("---------------------------");;

(* Testes *)
let testeDeAmbiente1 : ambiente = [("variavel1", ValorNumerico 1)];;
let testeDeAmbiente2 : ambiente = atualizaAmbiente testeDeAmbiente1 "variavel2" (ValorNumerico 2);;
let testeDeAmbiente3 : ambiente = atualizaAmbiente testeDeAmbiente2 "variavel3" (ValorBooleano false);;
lookup_ambiente testeDeAmbiente1 "variavel1";;
lookup_ambiente testeDeAmbiente1 "variavel2";;
lookup_ambiente testeDeAmbiente1 "variavel3";;
print_string ("\nTestes de Ambiente     [OK]\n");;

let valorA : valor = ValorNumerico 7;;
let valorB : valor = ValorBooleano true;;
print_string ("Testes de Valores      [OK]\n");;

let expressao1 : expressao = Numero 13;;
let expressao2 : expressao = Numero 22;;
print_string ("Testes de Expressoes   [OK]\n");;

let variavel1 : expressao = Var "variavel1";;
let variavelFalse : expressao = Var "variavel3";;
print_string ("Testes de Variaveis    [OK]\n");;

let testeOperacao1 : expressao = Op(expressao1, Soma, expressao2);;
let testeOperacao2 : expressao = Op(expressao2, Subt, expressao1);;
let testeOperacao3 : expressao = Op(expressao1, Mult, expressao2);;
let testeOperacao4 : expressao = Op(expressao2, Div, expressao1);;
let testeOperacao5 : expressao = Op(expressao1, Maior, expressao2);;
let testeOperacao6 : expressao = Op(expressao1, Menor, expressao2);;
let testeOperacao7 : expressao = Op(expressao1, MaiorIgual, expressao2);;
let testeOperacao8 : expressao = Op(expressao1, MenorIgual, expressao2);;
let testeOperacao9 : expressao = Op(expressao1, Igual, expressao2);;
let testeOperacao10 : expressao = Op(expressao1, Diferente, expressao2);;
print_string ("Testes de Operacoes    [OK]\n");;

let testeIf : expressao = If(variavelFalse, testeOperacao1, testeOperacao2);;
avalia testeDeAmbiente3 testeIf;;
print_string ("Testes de If Then Else [OK]\n");;

let ambienteComClosures : ambiente = [ ("teste1", ValorNumerico 10);("teste2", ValorNumerico 5) ];;

let testeAplicacao1 : expressao = Op(Numero(2), Menor, Numero(5));;
let testeAplicacao2 : expressao = Numero(7);;
let testeAplicacao3 : expressao = Lam("f", testeAplicacao1);;
let testeAplicacao4 : expressao = Aplicacao(testeAplicacao2, testeAplicacao3);;
avalia ambienteComClosures testeAplicacao4;;
print_string ("Testes de Aplicacao    [OK]\n");;

let testeLet1 : expressao = Op(Numero 7, Soma, Numero 3);;
let testeLet2 : expressao = Op(Var "teste1", Subt, Numero 3);;
let testeLet3 : expressao = Let("teste1", testeLet1, testeLet2);;
avalia ambienteComClosures testeLet3;;
print_string ("Testes de Let          [OK]\n");;

let testeLetRec1 = Op(Numero(13), Maior, Numero(10));;
let testeLetRec2 = Numero(7);;
let testeLetRec3 = Op(Var("x"), Mult, Aplicacao(Var("teste"), Op(Var("x"), Subt, Numero(1))));;
let testeLetRec4 = Var("teste1");;
let rec testeRec = (LetRec("teste1", "x", If(testeLetRec1, testeLetRec2, testeLetRec3), testeLetRec4));;
let fatorialDe5 = Aplicacao(testeRec, Numero 5);;
let letRecAmbiente : ambiente = [];;
avalia letRecAmbiente fatorialDe5;;
print_string ("Testes de Let Rec      [OK]\n");;


print_string ("---------------------------\n");;
print_string ("---------------------------\n");;
print_string ("---------------------------\n");;