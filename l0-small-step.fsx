(* Avaliador de expressões Small-Step para L0 *)

(* Termos de L0 *)
type Terms = 
    | True 
    | False 
    | If of Terms * Terms * Terms
    | Zero
    | Succ of Terms
    | Pred of Terms
    | IsZero of Terms

exception NoRuleApplies

(* Verificação de termos numéricos *)
let rec isnumerical t =
    match t with
    | Zero -> true
    | Succ(t1) -> isnumerical t1
    | _ -> false

(* Semântica Small-Step *)
let rec step t =
    match t with
    | If(True,t2,t3) ->
        t2
    | If(False,t2,t3) ->
        t3
    | If(t1,t2,t3) ->
        let t1' = step t1 in
            If(t1',t2,t3)

    | Succ(t1) ->
        let t1' = step t1 in
            Succ(t1')

    | Pred(Zero) ->
        Zero
    | Pred(Succ(nv)) when isnumerical(nv) ->
        nv
    | Pred(t1) ->
        let t1' = step t1 in
            Pred(t1')

    | IsZero(Zero) ->
        True
    | IsZero(Succ(nv)) when isnumerical(nv) ->
        False
    | IsZero(t1) ->
        let t1' = step t1 in
            IsZero(t1')
    | _ -> raise NoRuleApplies

(* Avaliação - Multistep *)
let rec eval t = 
    try let t' = step t in
        eval t'
    with NoRuleApplies -> t

