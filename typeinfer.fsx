(*--- Auxiliar functions ---*)

let rec union list1 list2 =
    match list1, list2 with
    | [], other | other, [] -> other
    | x::xs, y::ys when x < y -> x :: (union xs list2)
    | x::xs, y::ys -> y :: (union list1 ys)

(*--- Type variable control ---*)

let type_var_counter = ref 0

let get_type_var (it : int ref) =
  incr it
  sprintf "T%d" !it
  
(*--- L1 abstract syntax ---*)

type Operation =
  | Sum
  | Subt
  | Mult
  | Div

type Expression =
  | Num of int
  | Bool of bool
  | Op of Expression * Operation * Expression
  
type Type =
  | TInt
  | TBool
 
(*--- TypeInfer functions ---*)
  
let rec collectEqs (e : Expression) =
  match e with
  | Num x -> []
  | Bool x -> []
  | Op(x, op, y) ->
    // Collecting constraints from both operators
    let c1 = collectEqs x in
    let c2 = collectEqs y in
      let constraints = union c1 c2 in 
        let var1 = get_type_var type_var_counter in
        let var2 = get_type_var type_var_counter in
          union [(var1, "TInt");(var2, "TInt")] constraints


      
      


let list = collectEqs (Op(Num 10, Sum, Bool true))
list |> List.iter (fun x -> printf "%s: %s\n" (fst(x)) (snd(x)))