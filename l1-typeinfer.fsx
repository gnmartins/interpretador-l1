(*
    
    Trabalho Final 2016/2

    Grupo:

    Leonardo Silva Rosa     242292
    Marcelo Haider          220505
    Gabriel Martins         228416

    Inferencia de tipos : Explicita com Polimorfismo

*)

type Variavel = string

type Operador = 
    | Soma
    | Subtracao
    | Mult
    | Div
    | Menor
    | MenorIgual
    | Igual
    | Diferente
    | MaiorIgual
    | Maior

type Tipo = 
    | TAny
    | TVar          of int
    | TList         of Tipo
    | TInt
    | TBool
    | TFun          of Tipo * Tipo
and 
    Ambiente = (Variavel * Tipo) list

type Mapeamento = (Tipo * Tipo) list

type Expressao = 
    | Num           of int
    | Bool          of bool
    | Op            of Expressao * Operador * Expressao
    | If            of Expressao * Expressao * Expressao
    | Var           of Variavel
    | App           of Expressao * Expressao
    | Fn            of Variavel * Tipo * Expressao
    | Let           of Variavel * Tipo * Expressao * Expressao
    | LetRec        of Variavel * Tipo * Variavel * Tipo * Expressao * Expressao
    | Nil
    | Cons          of Expressao * Expressao
    | IsEmpty       of Expressao
    | Hd            of Expressao
    | Tl            of Expressao
    | Raise
    | TryWith       of Expressao * Expressao

(*-----------------------------------------------------------------------*)

(*--- Excecoes ---*)

exception VariableTypeNotDefinedException
exception BadlyTypedException

(*-----------------------------------------------------------------------*)    

(*--- AMBIENTE ---*)

let rec lookupAmbiente (ambiente: Ambiente) (x: Variavel) : Tipo =
     match ambiente with 
     | [] -> raise VariableTypeNotDefinedException
     | (variavel, tipo) :: tl ->
        if variavel = x then 
            tipo
        else
            lookupAmbiente tl x;;

let adicionaAmbiente (ambiente: Ambiente) (x: Variavel) (t : Tipo) : Ambiente =
    (x, t) :: ambiente

(*-----------------------------------------------------------------------*)

(*--- Coletor de constraints ---*)

let mutable count = 0

let rec collectEqs (ambiente: Ambiente) (e: Expressao) : Tipo * list<Tipo * Tipo> =

    count <- count + 1

    match e with

    | Num e -> (TInt, [])

    | Bool e -> (TBool, [])

    | Var e -> (lookupAmbiente ambiente e, [])

    | If (e1, e2, e3) ->
        let (t1, c1) = collectEqs ambiente e1 in
            let (t2, c2) = collectEqs ambiente e2 in
                let (t3, c3) = collectEqs ambiente e3 in
                    (t2, c1 @ c2 @ c3 @ [(t1, TBool); (t2, t3)])


    | Op (e1, op, e2) ->
        let (t1, c1) = collectEqs ambiente e1 in 
            let (t2, c2) = collectEqs ambiente e2 in
                match op with
                    | Soma -> (TInt, c1 @ c2 @ [(t1, TInt); (t2, TInt)])
                    | Subtracao -> (TInt, c1 @ c2 @ [(t1, TInt); (t2, TInt)])
                    | Mult -> (TInt, c1 @ c2 @ [(t1, TInt); (t2, TInt)])
                    | Div -> (TInt, c1 @ c2 @ [(t1, TInt); (t2, TInt)])
                    | Menor -> (TBool, c1 @ c2 @ [(t1, TInt); (t2, TInt)])
                    | MenorIgual -> (TBool, c1 @ c2 @ [(t1, TInt); (t2, TInt)])
                    | Igual -> (TBool, c1 @ c2 @ [(t1, TInt); (t2, TInt)])
                    | Diferente -> (TBool, c1 @ c2 @ [(t1, TInt); (t2, TInt)])
                    | MaiorIgual -> (TBool, c1 @ c2 @ [(t1, TInt); (t2, TInt)])
                    | Maior -> (TBool, c1 @ c2 @ [(t1, TInt); (t2, TInt)])
    
    | App (e1, e2) ->
        let prev = count in
            let (t1, c1) = collectEqs ambiente e1 in
                let (t2, c2) = collectEqs ambiente e2 in
                    (TVar prev, c1 @ c2 @ [t1, TFun (t2, TVar prev)])

    | Fn (x, t, e) ->
        let ambiente' = adicionaAmbiente ambiente x t in
            let (t1, c1) = collectEqs ambiente' e in
                (TFun (t, t1), c1)
    
    | Let (x, t, e1, e2) ->
        let ambiente' = adicionaAmbiente ambiente x t in
            let (t1, c1) = collectEqs ambiente e1 in
                let (t2, c2) = collectEqs ambiente' e2 in
                        (t2, c1 @ c2 @ [t, t1])

    | LetRec (f, tf, x, tx, e1, e2) ->
        let ambiente' = adicionaAmbiente ambiente f (TFun (tx, tf)) in
            let ambiente'' = adicionaAmbiente ambiente' x tx in
                let (t1, c1) = collectEqs ambiente'' e1 in
                    let (t2, c2) = collectEqs ambiente' e2 in
                        (t2, c1 @ c2 @ [tf, t1])

    | Nil -> (TList (TVar count), [])

    | Cons (e1, e2) ->
        let (t1, c1) = collectEqs ambiente e1 in
            let (t2, c2) = collectEqs ambiente e2 in
                (t2, c1 @ c2 @ [TList t1, t2])

    | Hd e ->
        let (t, c) = collectEqs ambiente e in
            (TVar count, c @ [t, TList (TVar count)])

    | Tl e ->
        let (t, c) = collectEqs ambiente e in
            (TList (TVar count), c @ [t, TList (TVar count)])

    | IsEmpty e ->
        let (t, c) = collectEqs ambiente e in
            (TBool, c @ [t, TList (TVar count)])

    | Raise -> (TVar count, [])

    | TryWith (e1, e2) ->
        let (t1, c1) = collectEqs ambiente e1 in
            let (t2, c2) = collectEqs ambiente e2 in
                (t2, c1 @ c2 @ [t1, t2])         

(*-----------------------------------------------------------------------*)

(*--- Resolvedor de constraints ---*)

let rec findVariable (t : Tipo) (var : Tipo) : bool =

    if t = var then
        true
    else
        match t with
            | TFun (t1, t2) -> findVariable t1 var || findVariable t2 var
            | TList (t1) -> findVariable t1 var
            | _ -> false    

let rec mapTipo (t : Tipo) (var : Tipo) (v : Tipo) : Tipo = 

    match t with
        | TVar n when t = var -> v
        | TFun (t1, t2) -> TFun (mapTipo t1 var v, mapTipo t2 var v)
        | TList t1 -> TList (mapTipo t1 var v)
        | _ -> t

let rec mapLista (c : list<Tipo * Tipo>) (var : Tipo) (t : Tipo) : list<Tipo * Tipo> = 

    if c.IsEmpty then
        []
    else
        let (t1, t2) = c.Head in
            [mapTipo t1 var t, mapTipo t2 var t] @ mapLista c.Tail var t
                     

let rec unify (c : list<Tipo * Tipo>) : Mapeamento = 

    if c.IsEmpty then
        []
    else
        let (t1, t2) = c.Head in
            if t1 = t2 then
                unify c.Tail
            else
                match (t1, t2) with
                    | (TVar n, t3) when not (findVariable t1 t3) -> (unify (mapLista c.Tail t1 t3)) @ [t1, t3]  
                    | (t3, TVar n) when not (findVariable t2 t3) -> (unify (mapLista c.Tail t2 t3)) @ [t2, t3]  
                    | (TFun (t3, t4), TFun (t5, t6)) -> unify (c.Tail @ [t3, t5] @ [t4, t6])
                    | (TList t3, TList t4) -> unify (c.Tail @ [t3, t4])
                    | (TAny, t3) -> unify c.Tail
                    | (t3, TAny) -> unify c.Tail
                    | _ -> raise BadlyTypedException


(*-----------------------------------------------------------------------*)

(*--- Resolução de tipos ---*)

let rec typeResolve (t : Tipo) (m : Mapeamento) : Tipo = 

    match t with
        | TFun (t1, t2) -> TFun (typeResolve t1 m, typeResolve t2 m)
        | TList t1 -> TList (typeResolve t1 m)
        | TVar n1 -> 
            let map m =
                (let (t2, t3) = m in
                    if t = t2 then
                        Some t3
                    else
                        None) in
                let nt = List.tryPick map m in
                    match nt with
                        | Some realtype -> realtype
                        | _ -> t
        | _ -> t


(*-----------------------------------------------------------------------*)

(*--- Inferência de tipos ---*)

let typeInfer (ambiente: Ambiente) (e: Expressao) : Tipo =
    let (tipo, equacoes) = collectEqs ambiente e in
        let mapeamento = unify equacoes in
            typeResolve tipo mapeamento

(*-----------------------------------------------------------------------*)

(*--- Testes ---*)

let ambiente = [];;

(* Funcao dec : decrementa entrada quanto a mesma for diferente de 0. *)

let aplicacao = App (Var "funcaorec", Num 3);;
let aplicacao2 = App (Var "funcaorec", Op (Var "x", Subtracao, Num 1));;
let funcao = If (Op (Var "x", Diferente, Num 0), aplicacao2, Bool true);;
let funcaorec = LetRec ("funcaorec", TBool, "x", TInt, funcao, aplicacao);;

typeInfer ambiente funcaorec;;

(* Funcao count usando polimorfismo : conta o numero de elementos de uma lista. *)

let primeiraChamada = App (Var "count", Cons (Num 2, Cons (Num 3, Nil)));;
let chamadaRecursiva = App (Var "count", Tl (Var "x"));;
let funcaoInterna = If (IsEmpty (Var "x"), Num 0, Op (Num 1, Soma, chamadaRecursiva));;
let funcaoRecursiva = LetRec ("count", TInt, "x", TList (TAny), funcaoInterna, primeiraChamada);;

typeInfer ambiente funcaoRecursiva;;

